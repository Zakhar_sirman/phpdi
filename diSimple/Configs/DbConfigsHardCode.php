<?php

namespace DiStudy\Configs;

use DiStudy\Interfaces\DbConfigInterface;

class DbConfigsHardCode implements DbConfigInterface
{
    public function getHostName(): string
    {
        return 'localhost';
    }

    public function getDbName(): string
    {
        return 'phpdi2';
    }

    public function getUserName(): string
    {
        return 'root';
    }

    public function getDbPassword(): string
    {
        return 'root';
    }

}