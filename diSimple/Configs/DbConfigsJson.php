<?php

namespace DiStudy\Configs;

use DiStudy\Interfaces\DbConfigInterface;

class DbConfigsJson implements DbConfigInterface
{

    private string $fileName;

    public function __construct($fileName) {
        $this->fileName = $fileName;
    }

    protected function getConfigs() {
        $configs = file_get_contents($this->fileName);
        return json_decode($configs, true)['configs'];
    }

    public function getHostName(): string
    {
        $configs = $this->getConfigs();

        return $configs['hostname'];
    }

    public function getDbName(): string
    {
        $configs = $this->getConfigs();

        return $configs['db_name'];
    }

    public function getUserName(): string
    {
        $configs = $this->getConfigs();
        return $configs['user'];
    }

    public function getDbPassword(): string
    {
        $configs = $this->getConfigs();
        return $configs['password'];
    }
}