<?php

namespace DiStudy\DbServices\Pdo;

use DiStudy\Configs\DbConfigsHardCode;
use DiStudy\Interfaces\ConnectionInterface;

class Connection implements ConnectionInterface
{
    private $dbConfigs;

    public function __construct(DbConfigsHardCode $configs) {
        $this->dbConfigs = $configs;
    }

    public function make () : \PDO {
       return new \PDO("mysql:host=" . $this->dbConfigs->getHostName() .
           ";dbname=". $this->dbConfigs->getDbName() , $this->dbConfigs->getUserName(), $this->dbConfigs->getDbPassword());
    }
}