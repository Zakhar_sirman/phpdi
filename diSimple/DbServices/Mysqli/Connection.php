<?php

namespace DiStudy\DbServices\Mysqli;

use DiStudy\Interfaces\ConnectionInterface;
use DiStudy\Interfaces\DbConfigInterface;

class Connection implements ConnectionInterface
{
    private $dbConfigs;

    public function __construct(DbConfigInterface $configs) {
        $this->dbConfigs = $configs;
    }

    public function make ()  {
        return new \mysqli($this->dbConfigs->getHostName(),
            $this->dbConfigs->getUserName(),$this->dbConfigs->getDbPassword(),
            $this->dbConfigs->getDbName());
    }
}