<?php

namespace DiStudy\DbServices\Mysqli;

use DiStudy\DbServices\Mysqli\Connection;
use DiStudy\Interfaces\ConnectionInterface;
use DiStudy\Interfaces\RepositoryInterface;

class Repository implements RepositoryInterface
{
    /**
     * @var $connection ConnectionInterface
     */
    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection) {
        $this->connection = $connection;
    }

    public function select($table) {

       $query =  $this->connection->make()->query('SELECT * FROM ' . $table);
       return $query->fetch_assoc();
    }
}