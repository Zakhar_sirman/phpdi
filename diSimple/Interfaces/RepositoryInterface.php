<?php

namespace DiStudy\Interfaces;

interface RepositoryInterface
{
    public function select($table);
}