<?php

namespace DiStudy\Interfaces;

interface ConnectionInterface {

    public function make();
}