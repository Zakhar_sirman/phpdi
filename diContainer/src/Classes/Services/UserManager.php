<?php

namespace App\Classes\Services;

use App\Classes\Libs\Mailer;

class UserManager
{
    private Mailer $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function create() {

        $this->mailer->sendMail();
    }

}