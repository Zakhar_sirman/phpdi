<?php


require __DIR__ . '/vendor/autoload.php';

$app = require __DIR__ . '/bootstrap/container.php';

$userManager = $app->get('UserManager');

$userManager->create();