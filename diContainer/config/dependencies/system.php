<?php


return [
    'UserManager' => function (\Psr\Container\ContainerInterface $c) {
        return new \App\Classes\Services\UserManager($c->get('Mailer'));
    },
    'Mailer' => DI\factory(function () {
        return new \App\Classes\Libs\Mailer();
    }),
    'ProductService' => Di\factory(function () {
        return new \App\Classes\Services\ProductService();
    })
];
